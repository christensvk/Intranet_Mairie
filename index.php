<?php
session_start();
require_once("fonctions.php");

//ssi l'utilisateur est du service informatique
if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1){

    //ajout d'une information
    if(isset($_POST['ajoutInformation'])){
        addInformation($_POST);
        header('location:index.php');
    }

    //suppression d'une information
    if(isset($_GET['delete'])){
        $idInformation = $_GET['delete'];
        deleteInformation($idInformation);
        header('location:index.php');
    }    

    //obtention des informations destinées à remplir le formulaire de modification
    if(isset($_GET['update'])){
        $idInformation = $_GET['update'];
        $information = getOneInformationById($idInformation);
    }    

    //modification d'une information
    if(isset($_POST['updateInformation'])){
        updateInformation($_POST);
        header('location:index.php');
    }

    //suppression d'une application d'un service
    if(isset($_POST['deleteAppParService'])){
        deleteApplicationParService($_POST);
        header('location:index.php');
    }    

    //suppression de l'application de tous les services
    if(isset($_POST['deleteApp'])){
        deleteApp($_POST);
        header('location:index.php');
    }

    //ajout d'une application
    if(isset($_POST['ajoutApp'])){
        addApplication($_POST);
        header('location:index.php');
    }

    //modification d'une application (url ou nom)
    if(isset($_POST['updateApp'])){
        updateApplication($_POST);
        header('location:index.php');
    }

    $services = getAllServices();
}

$allApps = getAllApplications();
$applications = intoArrayApplication(getApplications());
$informations = getInformationsInformatique();
$informationsGenerales = getInformationsGenerales();
$fete = getEphemeride();

//récupère toutes les annonces à la Une
$annoncesUrgentes = getAnnoncesUrgentes();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">


        <link rel="icon" href="../../favicon.ico">
        <title>Intranet</title>
        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link href="dist/css/style.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
        <!---------------------  SCRIPT RECHERCHE APPLICATION  --------------------->
        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myList a").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>         
        <!---------------------  SCRIPT RECHERCHE SERVICE  --------------------->
        <script>    
            $(document).ready(function(){
                $("#myInputService").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myListService li").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>      
        <!---------------------  SCRIPT EDITEUR WYSIWYG  --------------------->
        <script src="dist/js/editeurs.js"></script>

    </head>


    <body>


        <!---------------------  HEADER  --------------------->

        <div class="container-fluid jumbotron index" id="header" style="height:200px;">
            <div id="contentHeader">
                <img class="moissy" src="dist/img/logo_moissy.jpg">
                <h1>Bienvenue</h1>
                <p><?php echo(date('d-m-Y')."<br>".$fete); ?></p>
            </div>
        </div>



        <!---------------------  BARRE HORIZONTALE  --------------------->
        <div class="bar"></div>


        <!---------------------  MENU  --------------------->

        <div class="col-lg-8 col-lg-offset-2 jumbotron menu" >
            <a href="index.php" role="button" class="btn btn-default btn-circle btn-lg color1" ><span class="glyphicon glyphicon-home" id="home"></span></a>
            <a href="http://srv-appli-7/forum/index.php" role="button" class="btn btn-default btn-circle btn-lg color2" >Forum</a>
            <a href="annonces.php" role="button" class="btn btn-default btn-circle btn-lg color3" >Annonces</a>
            <?php if(!isset($_SESSION['identifiant'])): ?>
            <a href="authentification.php" role="button" class="btn btn-default btn-circle btn-lg color4" >Connexion</a>
            <?php else: ?>
            <a href="logout.php" role="button" class="btn btn-default btn-circle btn-lg color4" >Deconnexion</a>
            <?php endif; ?>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-lg-offset-2 col-md-4 col-xs-12 col-sm-4">
                    <div class="side">


                        <!--------------------- HORLOGE --------------------->
                        <div>
                            <div class="clock">
                                <div class="outer-clock-face">
                                    <div class="marking marking-one"></div>
                                    <div class="marking marking-two"></div>
                                    <div class="marking marking-three"></div>
                                    <div class="marking marking-four"></div>
                                    <div class="inner-clock-face">
                                        <div class="hand hour-hand"></div>
                                        <div class="hand min-hand"></div>
                                        <div class="hand second-hand"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <!--------------------- ANNONCES A LA UNE --------------------->

                        <?php if(isset($annoncesUrgentes) && !empty($annoncesUrgentes)): ?>

                        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom:10px;">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <?php for($i = 0 ; $i <= count($annoncesUrgentes) ; $i++ ):?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"></li>
                                <?php endfor; ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="col-xs-12">
                                <div class="carousel-inner">
                                    <div class="item active" id="firstSlide">
                                        <div class="carousel-content">
                                            <div>
                                                <h3>Annonces à la une</h3>
                                                <a href="annonces.php" >Retrouvez ici les toutes les annonces</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php foreach($annoncesUrgentes as $key):?>
                                    <?php 
                                    $titre = $key -> titre;
                                    if(strlen($titre)>17){
                                        $titre=substr($titre,0,17)."...";
                                    }
                                    ?>
                                    <div class="item">
                                        <div class="carousel-content">
                                            <div>
                                                <h3><a href="annonces.php?id=<?php echo $key -> id; ?>"><?php echo $titre; ?></a></h3>
                                                <p>Postée par <?php echo($key->name); ?> le <?php echo date("j/m/Y", strtotime($key->date)); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>


                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                        <?php endif; ?>

                        <!--------------------- RECHERCHE GOOGLE --------------------->
                        <div>
                            <form action="http://www.google.com/search" method="get">
                                <img class="logo" src="dist/img/google.png"><input type="text" name="q"/>
                                <input type="submit" value="Rechercher" />
                            </form>
                        </div>
                        <br>


                        <!--------------------- FLUX RSS --------------------->
                        <div class="feedgrabbr_widget" id="fgid_3c7201a47d31095ee366ad361"></div>
                        <script>
                            if (typeof (fg_widgets) === "undefined") 
                                fg_widgets = new Array(); 
                            fg_widgets.push("fgid_3c7201a47d31095ee366ad361");
                        </script>
                        <script async src="https://www.feedgrabbr.com/widget/fgwidget.js"></script>
                        <br>


                        <!--------------------- METEO --------------------->
                        <div id="cont_506cceddb2e2e1bb8c00dce714ca4ff4"><script type="text/javascript" async src="https://www.tameteo.com/wid_loader/506cceddb2e2e1bb8c00dce714ca4ff4"></script></div>
                    </div>
                </div>


                <!--------------------- INFORMATIONS --------------------->
                <div class="col-lg-6 col-md-8 col-xs-12 col-sm-8">
                    <h1 id="titreInfos"> <span class="glyphicon glyphicon-chevron-right"></span> Informations</h1>
                    <div id="informations">
                        <h1><span class="glyphicon glyphicon-info-sign"></span> Le système informatique vous informe</h1>
                        <div class="panel-group" id="accordion">
                            <?php foreach($informations as $key):?>
                            <div class="panel panel-default">
                                <a data-toggle="collapse" data-parent="#accordion" href="<?php echo("#collapse".$key->id); ?>">
                                    <div class="panel-heading" id="informatique">
                                        <h4 class="panel-title"><?php echo($key->titre); ?></h4>
                                    </div>
                                </a>
                                <div id="<?php echo("collapse".$key->id); ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php echo($key->details); ?>
                                    </div>
                                    <?php if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1): ?>
                                    <div class="panel-footer">
                                        <a href="index.php?delete=<?php echo $key->id ?>" onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $key->titre ?> ?'));" class="btn btn-default">Supprimer</a>
                                        <a href="index.php?update=<?php echo $key->id ?>" class="btn btn-default">Modifier</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <br>
                            <?php if(isset($informationsGenerales) && !empty($informationsGenerales)): ?>
                            <h1><span class="glyphicon glyphicon-info-sign"></span> La mairie vous informe</h1>
                            <div class="panel-group" id="accordion2">
                                <?php foreach($informationsGenerales as $key):?>
                                <div class="panel panel-default">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="<?php echo("#collapse".$key->id); ?>">
                                        <div class="panel-heading" id="mairie">
                                            <h4 class="panel-title"><?php echo($key->titre); ?></h4>
                                        </div>
                                    </a>
                                    <div id="<?php echo("collapse".$key->id); ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php echo($key->details); ?>
                                        </div>
                                    <?php if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1): ?>
                                    <div class="panel-footer">
                                        <a href="index.php?delete=<?php echo $key->id ?>" onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $key->titre ?> ?'));" class="btn btn-default">Supprimer</a>
                                        <a href="index.php?update=<?php echo $key->id ?>" class="btn btn-default">Modifier</a>
                                    </div>
                                    <?php endif; ?>                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>


                            <!---------------------MODIFICATION D'UNE INFORMATION --------------------->
                                                                <?php if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1): ?>
 <!-- Si l'utilisateur est du service informatique -->
                            <?php if(isset($information) && !empty($information)): ?>
                            <form method="post" class="infoForm color2 shadow" action="index.php">
                                <div class="form-group">
                                    <label for="titre">Titre: </label>
                                    <input type="text" class="form-control" name="titre" value="<?php echo($information[0]->titre); ?>" />
                                    <br>
                                </div>
                                <input type="button" value="G" style="font-weight:bold;" onclick="commande('bold');" >
                                <input type="button" value="I" style="font-style: italic;" onclick="commande('italic');" />
                                <input type="button" value="S" style="text-decoration: underline;" onclick="commande('underline');" />
                                <input type="button" value="Lien" onclick="commande('createLink');" />
                                <select onchange="commande('formatBlock', this.value); this.selectedIndex = 0;">
                                    <option value="">Style</option>
                                    <option value="<p>">Paragraphe</option>
                                    <option value="<h2>">Titre principal</option>
                                    <option value="<h3>">Titre secondaire</option>
                                    <option value="<h4>">Sous-titre</option>
                                </select>
                                <select onchange="commande('foreColor',this.value);this.selectedIndex = 0;">
                                    <option value="">Couleur</option>
                                    <option value="#555555">Défaut</option>
                                    <option class="noir" value="black">Noir</option>
                                    <option class="rouge" value="red">Rouge</option>
                                    <option class="couleur vert" value="green">Vert</option>
                                    <option class="couleur bleu" value="blue">Bleu</option>
                                </select>
                                <br>
                                <div class="form-control" id="editeurUpdate" contentEditable><?php echo($information[0]->details); ?></div>
                                <input type="hidden" id="detailsUpdate" class="form-control" name="details" value=""/>
                                <br>
                                <div class="form-group">
                                    <label for="idservice">Service: </label>
                                    <select name="idservice">
                                        <option value="1">Informatique</option>
                                        <option value="3">Information générale</option>
                                    </select>
                                </div>
                                <input type="hidden" class="form-control" name="id" value="<?php echo($information[0]->id); ?>" />
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-default" name="updateInformation" value="Modifier" onclick="resultatInfoUpdate()">  
                                </div>
                            </form>
                            <?php endif; ?>


                            <!--------------------- AJOUT D'UNE INFORMATION  --------------------->
                            <div class="form-actions">
                                <button data-toggle="collapse" class="btn btn-default" data-target="#addInformation">Ajouter une information</button>
                            </div>
                            <div id="addInformation" class="collapse">
                                <form class="infoForm lightgray shadow" method="post" action="index.php">
                                    <div class="form-group">
                                        <label for="titre">Titre: </label>
                                        <input type="text" class="form-control" name="titre" />
                                        <br>
                                    </div>
                                    <input type="button" value="G" style="font-weight:bold;" onclick="commande('bold');" >
                                    <input type="button" value="I" style="font-style: italic;" onclick="commande('italic');" />
                                    <input type="button" value="S" style="text-decoration: underline;" onclick="commande('underline');" />
                                    <input type="button" value="Lien" onclick="commande('createLink');" />
                                    <select onchange="commande('formatBlock', this.value); this.selectedIndex = 0;">
                                        <option value="">Style</option>
                                        <option value="<p>">Paragraphe</option>
                                        <option value="<h2>">Titre principal</option>
                                        <option value="<h3>">Titre secondaire</option>
                                        <option value="<h4>">Sous-titre</option>
                                    </select>
                                    <select onchange="commande('foreColor',this.value);this.selectedIndex = 0;">
                                        <option value="">Couleur</option>
                                        <option value="#555555">Défaut</option>
                                        <option class="noir" value="black">Noir</option>
                                        <option class="rouge" value="red">Rouge</option>
                                        <option class="vert" value="green">Vert</option>
                                        <option class="bleu" value="blue">Bleu</option>
                                    </select>
                                    <br>
                                    <div class="form-control" id="editeur" contentEditable></div>
                                    <input type="hidden" id="details" class="form-control" name="details" value=""/>
                                    <br>
                                    <div class="form-group">
                                        <label for="idservice">Service: </label>
                                        <select name="idservice">
                                            <option value="1">Informatique</option>
                                            <option value="3">Information générale</option>
                                        </select>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-default" name="ajoutInformation" value="Ajouter" onclick="resultatInfo()">  
                                    </div>
                                </form>
                                <br>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <hr>


                    <!--------------------- APPLICATIONS PAR SERVICE --------------------->
                    <h1><span class="glyphicon glyphicon-list"></span> Applications par service</h1>
                    <?php foreach($applications as $key => $value): ?>
                    <?php 
                    $words =  explode(" ",$key);
                    if(count($words)>1){
                        $cle = str_replace(" ","_",$key);
                    }else{
                        $cle=$key;
                    }
                    ?>
                    <?php if(count($value)>0): ?>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="<?php echo("#s_".$cle); ?>"><?php echo $key; ?></button>
                    <div id="<?php echo("s_".$cle); ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h3 class="modal-title">Applications du service <?php echo $key; ?></h3>
                                </div>
                                <div class="modal-body">
                                    <ul class="list-group" id="myList">
                                        <?php foreach($value as $apps): ?>
                                        <a class="list-group-item" style="margin-top:6px;" href="<?php echo $apps -> url; ?>"><?php echo $apps -> nom; ?></a>


                                        <!--------------------- SUPPRIMER UNE APPLICATION --------------------->
                                        <?php if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1): ?> <!-- Si l'utilisateur est du service informatique -->
                                        <form method="post" action="index.php" onSubmit="if(!confirm(' Supprimer l\'application <?php echo($apps -> nom); ?> du service <?php echo($key); ?>?')){return false;}" style="display: inline;">
                                            <input type="hidden" name="idapplication" value="<?php echo $apps->idapplication; ?>">                        
                                            <input type="hidden" name="idservice" value="<?php echo $apps->id; ?>">
                                            <input type="submit" class="btn lightgray" name="deleteAppParService" value="Supprimer "> 
                                        </form>
                                        <form method="post" action="index.php" onSubmit="if(!confirm(' Supprimer l\'application <?php echo($apps -> nom); ?> de tous les services ?')){return false;}" style="display: inline;">
                                            <input type="hidden" name="idapplication" value="<?php echo $apps->idapplication; ?>">
                                            <input type="submit" class="btn lightgray" name="deleteApp" value="Supprimer de tous les services"> 
                                        </form>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>  
                    <?php endforeach; ?>
                    <hr>


                    <!--------------------- RECHERCHER UNE APPLICATION --------------------->
                    <div id="rechercheApp">
                        <h1><span class="glyphicon glyphicon-search"></span> Rechercher une application</h1>
                        <div id="listeApps">
                            <input class="form-control" id="myInput" type="text" placeholder="Entrer le nom d'une application">
                            <br>
                            <ul class="list-group" id="myList"> <!-- Liste des applications -->
                                <?php foreach($allApps as $key): ?>
                                <a href="<?php echo $key -> url ?>" class="list-group-item" ><?php echo $key -> nom; ?></a>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>


                    <!--------------------- AJOUTER UNE APPLICATION --------------------->
                    <?php if(isset($_SESSION['idservice']) && $_SESSION['idservice']==1): ?><!-- Si l'utilisateur est du service informatique -->
                    <div id="crudApp">
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group">
                                <button class="btn btn-default btn-lg blue" data-toggle="collapse" data-target="#updateApp">Modifier une application</button>
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-default btn-lg blue" data-toggle="collapse" data-target="#addApp">Ajouter une application</button>
                            </div>
                        </div>
                        <div id="addApp" class="collapse">
                            <form method="post" class="infoForm shadow" action="index.php">
                                <div class="form-group">
                                    <label for="nom">Nom: </label>
                                    <input type="text" class="form-control" name="nom" />
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="url">URL: </label>
                                    <input type="text" class="form-control" name="url" />
                                </div>
                                <label for="service">Service(s): </label>
                                <input class="form-control" id="myInputService" type="text" placeholder="Entrer le nom d'un service">
                                <ul class="list-group" id="myListService">
                                    <?php foreach($services as $service): ?>
                                    <li>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="service[]" value="<?php echo($service->id); ?>" /><?php echo($service->nom); ?></label>
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-default" name="ajoutApp" value="Ajouter">  
                                </div>
                            </form>
                        </div>


                        <!--------------------- MODIFIER UNE APPLICATION --------------------->
                        <div id="updateApp" class="collapse infoForm shadow white">
                            <?php foreach($allApps as $key): ?>
                            <h3><?php echo($key -> nom); ?></h3>
                            <form method="post" class="form-inline" action="index.php" onSubmit="if(!confirm(' Modifier l\'application <?php echo($key -> nom); ?> ?')){return false;}">
                                <div class="form-group">
                                    <label for="nom">Nom: </label>
                                    <input type="text" class="form-control" name="nom" value="<?php echo $key -> nom ?>"/>
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="url">URL: </label>
                                    <input type="text" class="form-control" name="url" value="<?php echo $key -> url ?>"/>
                                </div>
                                <input type="hidden" name="id" value="<?php echo($key -> id); ?>">
                                <input type="submit" class="btn btn-success" name="updateApp" value="Modifier">
                                <hr>
                            </form>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <script>
            const secondHand = document.querySelector('.second-hand');
            const minsHand = document.querySelector('.min-hand');
            const hourHand = document.querySelector('.hour-hand');

            function setDate() {
                const now = new Date();

                const seconds = now.getSeconds();
                const secondsDegrees = ((seconds / 60) * 360) + 90;
                secondHand.style.transform = `rotate(${secondsDegrees}deg)`;

                const mins = now.getMinutes();
                const minsDegrees = ((mins / 60) * 360) + ((seconds/60)*6) + 90;
                minsHand.style.transform = `rotate(${minsDegrees}deg)`;

                const hour = now.getHours();
                const hourDegrees = ((hour / 12) * 360) + ((mins/60)*30) + 90;
                hourHand.style.transform = `rotate(${hourDegrees}deg)`;
            }

            setInterval(setDate, 1000);

            setDate();        
        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="dist/js/bootstrap.min.js"></script>
    </body>
</html>