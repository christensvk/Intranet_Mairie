<?php

require_once 'connexion.php';

/****** AUTHENTIFICATION ******/

//retourne les informations selon les données de connexion entrées
function getAuthentification($login,$pass){
    global $pdo;
    
    $query="SELECT * 
            FROM Personnel 
            WHERE login=:login 
            AND password=:pass;";
    
    $prep=$pdo->prepare($query);
    $prep->bindValue(':login',$login);
    $prep->bindValue(':pass',$pass);
  
    $prep->execute();
    
    if($prep->rowCount()==1){
        $result=$prep->fetch(PDO::FETCH_ASSOC);
        return $result;
    }else{
        return false;
    }
}


/****** INFORMATIONS ******/

//Retourne les informations du service informatique
function getInformationsInformatique() {
    global $pdo;
    
    $query = 'SELECT * 
              FROM Information
              WHERE idservice = 1;'; // /!\ modifiable /!\
    
    $prep = $pdo->prepare($query);
  
    $prep->execute();
  
    return $prep->fetchAll();
}

//Retourne les informations générales
function getInformationsGenerales() {
    global $pdo;
    
    $query = 'SELECT * 
              FROM Information
              WHERE idservice = 3;'; // /!\ modifiable /!\
    
    $prep = $pdo->prepare($query);
  
    $prep->execute();
  
    return $prep->fetchAll();
}

//Ajoute une information
function addInformation($params){
    global $pdo;

    $titre=$params['titre'];
    $details=$params['details'];
    $idservice=$params['idservice'];

    $requete="INSERT INTO Information (titre, details, idservice) 
              VALUES (:titre, :details, :idservice);";

    $prep=$pdo->prepare($requete);

    $prep->bindValue(':titre', $titre, PDO::PARAM_STR);
    $prep->bindValue(':details', $details, PDO::PARAM_STR);
    $prep->bindValue(':idservice', $idservice, PDO::PARAM_INT);  
    $prep->execute();
}

//Retourne une information en fonction de l'id de l'information (pour la modification)
function getOneInformationById($id){
    global $pdo;
    
    $query = 'SELECT * 
              FROM Information 
              WHERE id=:id;';

    $prep = $pdo->prepare($query);

    $prep->bindValue(':id', $id, PDO::PARAM_INT);
  
    $prep->execute();
  
    return $prep->fetchAll();    
}

//Modifie une information
function updateInformation($param){
    global $pdo;

    $id = $param['id'];
    $titre = $param['titre'];
    $details = $param['details'];
    $idservice = $param['idservice'];

    $requete="UPDATE Information 
              SET titre=:titre, details = :details, idservice = :idservice 
              WHERE id=:id;";

    $prep=$pdo->prepare($requete);

    $prep->bindValue(':titre',$titre,PDO::PARAM_STR);
    $prep->bindValue(':details',$details,PDO::PARAM_STR);
    $prep->bindValue(':idservice',$idservice,PDO::PARAM_STR);
    $prep->bindValue(':id',$id,PDO::PARAM_INT);

    $prep->execute();
}

//Supprime une information en fonction de l'id
function deleteInformation($idInformation){
    global $pdo;
    
    $query = "DELETE 
              FROM Information 
              WHERE id = :id;";
    try {
        $prep = $pdo->prepare($query);
      
        $prep->bindValue(':id', $idInformation);
      
        $prep->execute();
    }
    catch ( Exception $e ) {
        die ("erreur dans la requete ".$e->getMessage());
    }    
}


/****** ANNONCES ******/

//Retourne toutes les annonces, leur auteur
function getAnnonces() {
    global $pdo;
    
    $query = 'SELECT 
                Annonce.id, 
                titre,
                description,
                date,
                Personnel.nom as name 
              FROM 
                Annonce,
                Personnel 
              WHERE Personnel.id=Annonce.idpersonnel 
              ORDER BY 
                date DESC,
                Annonce.id;';

    $prep = $pdo->prepare($query);
   
    $prep->execute();
  
    return $prep->fetchAll();
}

//Retourne toutes les annonces urgentes
function getAnnoncesUrgentes() {
    global $pdo;
    
    $query = 'SELECT 
                Annonce.id, 
                titre,
                description,
                date,
                Personnel.nom as name 
              FROM 
                Annonce,
                Personnel 
              WHERE Personnel.id=Annonce.idpersonnel 
              AND urgent = 1
              ORDER BY 
                date DESC,
                Annonce.id;';

    $prep = $pdo->prepare($query);
   
    $prep->execute();
  
    return $prep->fetchAll();
}

//Retourne les annonces en fonction de l'id du personnel (mes annonces)
function getAnnoncesById($id) {
    global $pdo;
    
    $query = 'SELECT 
                Annonce.id, 
                titre,
                description,
                date,
                Personnel.nom as name 
              FROM 
                Annonce,
                Personnel
              WHERE
                Personnel.id=Annonce.idpersonnel
              AND
                idpersonnel=:id 
              ORDER BY 
                date DESC;';
    
    $prep = $pdo->prepare($query);
    
    $prep->bindValue(':id', $id, PDO::PARAM_INT);
   
    $prep->execute();
  
    return $prep->fetchAll();
}

//Retourne une annonce en fonction de l'id de l'annonce (pour la modification)
function getOneAnnonceById($id){
    global $pdo;
    
    $query = 'SELECT 
                Annonce.id, 
                titre,
                description,
                urgent
              FROM 
                Annonce
              WHERE 
                id=:id;';
    
    $prep = $pdo->prepare($query);
    
    $prep->bindValue(':id', $id, PDO::PARAM_INT);
    
    $prep->execute();
  
    return $prep->fetchAll();    
}

//Ajoute une annonce
function addAnnonce($params){
    global $pdo;

    $titre=$params['titre'];
    $description=$params['description'];
    $date=$params['date'];
    $idpersonnel=$params['idpersonnel'];
    $urgence=$params['urgent'];
    
    $requete="INSERT INTO Annonce (titre, description, date, idpersonnel, urgent) 
              VALUES (:titre, :description, :date, :idpersonnel, :urgence);";

    $prep=$pdo->prepare($requete);

    $prep->bindValue(':titre', $titre, PDO::PARAM_STR);
    $prep->bindValue(':description', $description, PDO::PARAM_STR);
    $prep->bindValue(':date', $date, PDO::PARAM_STR);  
    $prep->bindValue(':idpersonnel', $idpersonnel, PDO::PARAM_INT);  
    $prep->bindValue(':urgence', $urgence, PDO::PARAM_INT);  

    $prep->execute();
}

//Supprime une annonce en fonction de l'id
function deleteAnnonce($idAnnonce){
    global $pdo;
    
    $query = "DELETE 
              FROM Annonce 
              WHERE id = :id;";
    
    try {
        $prep = $pdo->prepare($query);
        $prep->bindValue(':id', $idAnnonce);
        $prep->execute();
    }
    catch ( Exception $e ) {
        die ("erreur dans la requete ".$e->getMessage());
    }    
}

//Modifie une annonce en fonction de l'id
function updateAnnonce($param){
    global $pdo;

    $id = $param['id'];
    $titre = $param['titre'];
    $description = $param['description'];
    $urgent = $param['urgent'];

    $requete="UPDATE Annonce 
              SET 
                titre=:titre, 
                description = :description,
                urgent = :urgent
              WHERE id=:id;";

    $prep=$pdo->prepare($requete);

    $prep->bindValue(':titre',$titre,PDO::PARAM_STR);
    $prep->bindValue(':description',$description,PDO::PARAM_STR);
    $prep->bindValue(':id',$id,PDO::PARAM_INT);
    $prep->bindValue(':urgent', $urgent, PDO::PARAM_INT);  

    $prep->execute();
}

//Trie les annonces
function triAnnonces(){
    global $pdo;

    $query = 'SELECT 
                Annonce.id, 
                titre,
                description,
                date,
                Personnel.nom as name 
              FROM 
                Annonce,
                Personnel 
              WHERE 
                Personnel.id=Annonce.idpersonnel 
              ORDER BY 
                date ASC;';
  
    $prep = $pdo->prepare($query);

    $prep->execute();
    return $prep->fetchAll();   
}

/******* Applications *******/

//Retourne toutes les applications
function getAllApplications(){
    global $pdo;
    
    $query = "SELECT * 
              FROM Application
              ORDER BY nom;";
        
    $prep = $pdo -> prepare($query);
    
    $prep -> execute();
    
    return $prep -> fetchAll();
}

//Retourne les applications et leur(s) service(s)
function getApplications(){
    global $pdo;
    
    $query='SELECT
              Service.id,
              Service.nom as service, 
              Application.nom, 
              Application.url, 
              Application.id as idapplication 
            FROM 
              ApplicationParService,
              Service,
              Application 
            WHERE 
              Application.id=ApplicationParService.idapplication 
            AND 
              ApplicationParService.idservice=Service.id;';
    
    $prep = $pdo->prepare($query);
   
    $prep->execute();
    return $prep->fetchAll();    
}

//Retourne un tableau associatif comportant les applications en fonction du service (section applications par service)
function intoArrayApplication($applications){
    $services = getAllServices();
    
    $apps = array();
    
    foreach($services as $key){
        ${$key->nom} = array();
        foreach($applications as $cle){
            if($cle -> id == $key -> id){
                array_push(${$key->nom},$cle);
            }
        }
        $apps[$key->nom]=${$key->nom};
    }
    return $apps;
}

//Supprime une application d'un service
function deleteApplicationParService($params){
    global $pdo;
  
    $idapplication = $params['idapplication'];
    $idservice = $params['idservice'];
  
    $query = "DELETE
              FROM `ApplicationParService`
              WHERE `idapplication` = :idapplication
              AND `idservice` = :idservice;";
    
    try {
        $prep = $pdo->prepare($query);
        $prep->bindValue(':idapplication', $idapplication,PDO::PARAM_INT);
        $prep->bindValue(':idservice', $idservice,PDO::PARAM_INT);
        $prep->execute();
    } catch ( Exception $e ) {
        die ("erreur dans la requete ".$e->getMessage());
    }
}

//Supprime une application entièrement (et de tous les services)
function deleteApp($params){
    global $pdo;
    
    $idapplication = $params['idapplication'];
    
    //supprime l'application de la table ApplicationParService
    $query = "DELETE 
              FROM `ApplicationParService` 
              WHERE `idapplication` = :idapplication;";
  
    try {
        $prep = $pdo->prepare($query);
        $prep->bindValue(':idapplication', $idapplication,PDO::PARAM_INT);
        $prep->execute();
    }
    catch ( Exception $e ) {
        die ("erreur dans la requete ".$e->getMessage());
    }  
    
    //supprime l'application de la table Application
    $secondquery = "DELETE 
                    FROM `Application` 
                    WHERE `id` = :idapplication;";
    
    try {
        $prep = $pdo->prepare($secondquery);
        $prep->bindValue(':idapplication', $idapplication,PDO::PARAM_INT);
        $prep->execute();
    }
    catch ( Exception $e ) {
        die ("erreur dans la requete ".$e->getMessage());
    }    
}

//Ajoute une application
function addApplication($params){
    global $pdo;

    $nom = $params['nom'];
    $url = $params['url'];
    $services = $params['service'];
    
    //vérifie si l'application existe déjà dans la table Application
    $checkquery = "SELECT * 
                  FROM Application 
                  WHERE nom = :nom 
                  AND url = :url;";
    
    try {
        $prep=$pdo->prepare($checkquery);

        $prep->bindValue(':nom', $nom, PDO::PARAM_STR);
        $prep->bindValue(':url', $url, PDO::PARAM_STR);

        $prep->execute();
        
        //si l'application n'existe pas
        if($prep->rowCount()==0){
            
            // 1. on l'insère dans la table Application
            $query = "INSERT INTO Application(nom,url) 
                      VALUES (:nom,:url);";

            try {
                $prep=$pdo->prepare($query);

                $prep->bindValue(':nom', $nom, PDO::PARAM_STR);
                $prep->bindValue(':url', $url, PDO::PARAM_STR);

                $prep->execute();
            } catch ( Exception $e ) {
                die ($e->getMessage());
            }
            
            // 2. on ajoute l'application dans tous les services cochés dans le formulaire
            foreach($services as $service){
                $secondquery = "INSERT INTO ApplicationParService(
                                    idapplication,
                                    idservice            
                                    )
                                    SELECT 
                                        Application.id,
                                        Service.id
                                    FROM 
                                        Application,
                                        Service
                                    WHERE 
                                        Application.nom = :nom
                                    AND
                                        Service.id = :service;";
                try {
                    $prep=$pdo->prepare($secondquery);

                    $prep->bindValue(':nom', $nom, PDO::PARAM_STR);
                    $prep->bindValue(':service', $service, PDO::PARAM_INT);

                    $prep->execute();
                                        
                } catch ( Exception $e ) {
                    die ($e->getMessage());
                }
            }
        } else {
            //si l'application existe déjà dans la table Application
            $application = $prep->fetchAll();
            $id = $application[0]->id;
            
            //vérification pour chaque service si l'application y est présente
            foreach($services as $service){
                $requete = "SELECT * 
                            FROM ApplicationParService 
                            WHERE idservice = :idservice 
                            AND idapplication = :idapplication;";
                
                $prep = $pdo->prepare($requete);
                
                $prep->bindValue(':idservice', $service, PDO::PARAM_INT);
                $prep->bindValue(':idapplication', $id, PDO::PARAM_INT);
                
                $prep -> execute();
                
                //insertion si elle n'est pas présente
                if($prep->rowCount()==0){
                    $lastquery = "INSERT INTO ApplicationParService(idapplication,idservice)
                                  VALUES(:idapplication,:idservice);";
                        
                    $prep = $pdo->prepare($lastquery);

                    $prep->bindValue(':idservice', $service, PDO::PARAM_INT);
                    $prep->bindValue(':idapplication', $id, PDO::PARAM_INT);

                    $prep -> execute();
                } else { return; }
            }
        }
    } catch ( Exception $e ) {
        die ($e->getMessage());
    }
}

//Modifie le nom et/ou l'url d'une application en fonction de l'id
function updateApplication($params){
    global $pdo;
    
    $id = $params['id'];
    $nom = $params['nom'];
    $url = $params['url'];    
    
    $requete="UPDATE Application 
              SET url=:url, nom = :nom 
              WHERE id=:id;";

    $prep=$pdo->prepare($requete);

    $prep->bindValue(':url',$url,PDO::PARAM_STR);
    $prep->bindValue(':nom',$nom,PDO::PARAM_STR);
    $prep->bindValue(':id',$id,PDO::PARAM_INT);

    $prep->execute();
}

/****** SERVICES ******/

//retourne tous les services
function getAllServices(){
  global $pdo;
  
  $query = "SELECT * 
            FROM Service
            WHERE `nom`<>:general
            ORDER BY nom;";
  
  $prep=$pdo->prepare($query);
  $prep->bindValue(':general',"General",PDO::PARAM_STR);

  $prep->execute();
  
  return $prep -> fetchAll();  
}

/****** EPHEMERIDE ******/

//retourne l'éphéméride 
function getEphemeride() {
  $document = new DomDocument();
  
  //chargement du flux rss
  $document->load('http://www.ephemeride-jour.fr/rss/rss_saints_jour.php');
  
  $elements = $document->getElementsByTagName('channel');
  $element = $elements->item(0);
  $enfants = $element->childNodes;
  
  $desc = $enfants[9]->textContent;
  
  if(isset($desc) && !empty($desc)){  
    $fete = explode(" ", $desc);
    $phrase = "Fête du jour: ".substr($fete[count($fete)-1],0,-1);
    return $phrase;
  } else {
    return null;
  }
}


?>
