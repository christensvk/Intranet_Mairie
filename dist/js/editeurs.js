function commande(nom, argument){
    if (typeof argument === 'undefined') {
        argument = '';
    }

    switch(nom){
        case "createLink":
            argument = prompt("Quelle est l'adresse du lien ?");
            
            if(argument.length == 0){
                argument = '';
            }

            break;
    }

    document.execCommand(nom, false, argument);
}

function resultat(){
    document.getElementById("description").value = document.getElementById("editeur").innerHTML;
}    

function resultatUpdate(){
    document.getElementById("descriptionUpdate").value = document.getElementById("editeurUpdate").innerHTML;
}    

function resultatInfo(){
    document.getElementById("details").value = document.getElementById("editeur").innerHTML;
}        

function resultatInfoUpdate(){
    document.getElementById("detailsUpdate").value = document.getElementById("editeurUpdate").innerHTML;
}

