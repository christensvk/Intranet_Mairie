<?php
session_start();
require_once("fonctions.php");

if(isset($_GET['id'])){
    $annonces = getOneAnnonceById($_GET['id']);
} else {
    //récupère toutes les annonces
    $annonces = getAnnonces();
}

if(isset($_SESSION['idservice'])){
    //ajoute une annonce
    if(isset($_POST['ajoutAnnonce'])){
        addAnnonce($_POST);
        header('location:mesannonces.php?id='.$_SESSION['identifiant']);
    }    
}

//trie les annonces
if(isset($_POST['triAnnonces'])){
    if($_POST['tri']=="ancien"){
        $annonces = triAnnonces();
    }
}

if($_SESSION['idservice']==1){
    //supprime une annonce 
    if(isset($_GET['delete'])){
        $idAnnonce = $_GET['delete'];
        deleteAnnonce($idAnnonce);
        header('location:annonces.php');
    }
}    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Intranet</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="dist/css/style.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>


        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myList li").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>

        <script src="dist/js/editeurs.js"></script>

    </head>

    <body style="background-color:#d9d9d9;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 jumbotron" id="header">
                    <div id="contentHeader">
                        <h1>Annonces</h1>
                        <p><?php echo date('d-m-Y');  ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="bar"></div>

        <div class="col-lg-8 col-lg-offset-2 jumbotron menu white">
            <a href="index.php" role="button" class="btn btn-default btn-circle btn-lg color1"><span class="glyphicon glyphicon-home" id="home"></span></a>
            <a href="http://srv-appli-7/forum/index.php" role="button" class="btn btn-default btn-circle btn-lg color2">Forum</a>
            <a href="annonces.php" role="button" class="btn btn-default btn-circle btn-lg color3">Annonces</a>
            <?php if(!isset($_SESSION['identifiant'])): ?>
            <a href="authentification.php" role="button" class="btn btn-default btn-circle btn-lg color4">Connexion</a>
            <?php else: ?>
            <a href="logout.php" role="button" class="btn btn-default btn-circle btn-lg color4">Deconnexion</a>
            <?php endif; ?>            
        </div>    

        <div class="container-fluid">
            <div class="row">
                <?php if(isset($_SESSION['idservice'])): ?> <!-- S'il y a bien un utilisateur connecté -->

                <div class="col-lg-4 col-lg-offset-4" style="margin-bottom: 15px; text-align:center;">

                    <button data-toggle="collapse" data-target="#demo" class="btn btn-danger color4">Ajouter une annonce</button>

                    <a class="btn btn-danger color4" href="mesannonces.php">Voir mes annonces</a>

                    <div id="demo" class="collapse shadow margintop">
                        <h3 class="color4"><span class="glyphicon glyphicon-plus"></span> Ajouter une annonce</h3>
                        <form method="post" action="annonces.php" class="infoForm white">
                            <div class="form-group">
                                <label for="titre">Titre: </label>
                                <input type="text" class="form-control" name="titre" />
                                <br>
                            </div>
                            
                            A la Une: 
                            <div class="radio">
                                <label><input type="radio" name="urgent" value="1">Oui</label>
                            </div>                            
                            <div class="radio">
                                <label><input type="radio" name="urgent" value="0" checked>Non</label>
                            </div>
                            
                            <input type="button" value="G" style="font-weight:bold;" onclick="commande('bold');" >
                            <input type="button" value="I" style="font-style: italic;" onclick="commande('italic');" />
                            <input type="button" value="S" style="text-decoration: underline;" onclick="commande('underline');" />
                            <input type="button" value="Lien" onclick="commande('createLink');" />
                            <select onchange="commande('foreColor',this.value);this.selectedIndex = 0;">
                                <option value="">Couleur</option>
                                <option value="#555555">Défaut</option>
                                <option class="noir" value="black">Noir</option>
                                <option class="rouge" value="red">Rouge</option>
                                <option class="couleur vert" value="green">Vert</option>
                                <option class="couleur bleu" value="blue">Bleu</option>
                            </select>                     
                            <br>

                            <div class="form-control" id="editeurUpdate" contentEditable ></div>

                            <input type="hidden" id="descriptionUpdate" class="form-control" name="description" value=""/>

                            <input type="hidden" class="form-control" name="date" value="<?php echo date("Y-m-d"); ?>" />
                            <br>

                            <input type="hidden" class="form-control" name="idpersonnel" value="<?php echo $_SESSION['identifiant']; ?>"/>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-default" name="ajoutAnnonce" onclick="resultatUpdate()"> 
                            </div>
                        </form>
                    </div>

                </div>          

                <?php endif; ?> 

                <div class="col-lg-8 col-lg-offset-2" style=" padding:30px 30px 30px 30px;; background-color:#deeaee; box-shadow: 0 5px 20px grey; margin-bottom:10px;">
                    <b><span class="glyphicon glyphicon-sort"></span> Trier: </b>
                    <form method="post" action="annonces.php">
                        <select name="tri">
                            <option value="recent">Du plus récent au plus ancien</option>
                            <option value="ancien">Du plus ancien au plus récent</option>
                        </select>
                        <input type="submit" value="Ok" name="triAnnonces">
                    </form>              

                    <br><b><span class="glyphicon glyphicon-search"></span> Rechercher une annonce:  </b> 
                    <input class="form-control" id="myInput" type="text" placeholder="Entrer un mot-clé, une date, un nom,...">
                    <br>
                    <ul class="list-group" id="myList">
                        <?php foreach($annonces as $key):?>
                        <li class="list-group-item" style="margin-bottom:20px; padding-top: 20px;padding-bottom: 20px; box-shadow: 5px 5px 15px grey;">   
                            <div class="media">
                                <div class="media-left media-middle">
                                    <img src="question_mark_2.png" class="media-object" style="width:100px">
                                </div>
                                <div class="media-body">
                                    <h3 class="media-heading"><?php echo($key->titre); ?></h3>
                                    <hr>
                                    <p><b><i>Postée par <?php echo($key->name); ?> le <?php echo date("j/m/Y", strtotime($key->date)); ?></i></b></p>
                                    <p><?php echo($key->description); ?></p>
                                    <?php if(isset($_SESSION['identifiant']) && $_SESSION['identifiant']==1): ?>
                                    <a href="annonces.php?delete=<?php echo $key->id ?>" onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $key->titre ?> ?'));" class="btn btn-danger">Supprimer</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li> 
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="dist/js/bootstrap.min.js"></script>
    </body>
</html>
