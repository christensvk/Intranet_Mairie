<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>INTRANET</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="dist/css/style.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
    </head>

    <body style="background-color:#EEEEEE">
        <div class="container">
            <div style="height:500px; 
                        width: 500px; 
                        border-radius:250px; 
                        background-color:#f2f2f2; 
                        background: linear-gradient(to bottom, grey, white); 
                        box-shadow: 5px 10px 30px #595959;
                        position: absolute;
                        top:0;
                        bottom: 0;
                        left: 0;
                        right:0;
                        margin: auto;
                        text-align:center;">
                <div style="margin-top: 125px; 
                            width: 400px; 
                            margin-right:auto; 
                            margin-left:auto;">
                    <h1 style="letter-spacing:2px;
                               color:#404040;">Connexion</h1>
                    <br>
                    <form action="login.php" method="post">

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" name="login" placeholder="Identifiant">
                        </div>

                        <br>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" name="pwd" placeholder="Mot de passe">
                        </div>

                        <br>

                        <button type="submit" class="btn btn-default">Connexion</button>

                    </form>
                    <br>
                    <a href="index.php" class="btn btn-primary" role="button">Intranet</a>
                </div>
            </div>
        </div>
    </body>
</html>