<?php
require_once 'fonctions.php';

if(isset($_POST['login']) && isset($_POST['pwd']) && !empty($_POST['login']) && !empty($_POST['pwd'])){
    $result = getAuthentification($_POST['login'],$_POST['pwd']);
    if($result){
        session_start();
        $_SESSION['nom']=$result['nom'];
        $_SESSION['identifiant']=$result['id'];
        $_SESSION['idservice']=$result['idservice'];
        header('location:index.php');
    }else{
         header('location:authentification.php');
    }
}else{
    header('location:authentification.php');
}
?>